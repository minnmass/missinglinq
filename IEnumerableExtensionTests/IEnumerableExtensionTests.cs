﻿using MissingLinq;
using MissingLinqTests.Helpers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MissingLinqTests {
	public class IEnumerableExtensionTests {
		[Fact]
		public static void MaxBy_Class_ReturnsNullOnEmptyList() {
			var actual = new List<ArbitraryClass>().MaxBy(i => i.ID);

			Assert.Null(actual);
		}

		[Fact]
		public static void MaxBy_Struct_ReturnsDefaultOnEmptyList() {
			var actual = new List<ArbitraryStruct>().MaxBy(i => i.ID);

			Assert.Equal(default(ArbitraryStruct), actual);
		}

		[Fact]
		public static void MaxBy_Class_SelectsMaximumBySelector() {
			var list = new List<ArbitraryClass> {
				new ArbitraryClass {
					Name = "b",
					ID = 1
				},
				new ArbitraryClass {
					Name = "a",
					ID = 2
				}
			};
			var expectedName = "a";

			var actualName = list.MaxBy(i => i.ID).Name;

			Assert.Equal(expectedName, actualName);
		}

		[Fact]
		public static void MaxBy_Struct_SelectsMaximumBySelector() {
			var list = new List<ArbitraryStruct> {
				new ArbitraryStruct {
					Name = "b",
					ID = 1
				},
				new ArbitraryStruct {
					Name = "a",
					ID = 2
				}
			};
			var expectedName = "a";

			var actualName = list.MaxBy(i => i.ID).Name;

			Assert.Equal(expectedName, actualName);
		}

		[Fact]
		public static void MaxBy_DuplicateEntries_ReturnsFirst() {
			var list = new List<ArbitraryClass> {
				new ArbitraryClass {
					Name = "a",
					ID = 1
				},
				new ArbitraryClass{
					Name = "b",
					ID = 2
				},
				new ArbitraryClass {
					Name = "c",
					ID = 2
				}
			};
			var expectedName = "b";

			var actualName = list.MaxBy(i => i.ID).Name;

			Assert.Equal(expectedName, actualName);
		}

		[Fact]
		public static void MaxBy_CustomComparerUsed() {
			var comparer = new Mock<IComparer<int>>();
			// add two objects, since a one-element list short-circuts the comparison because it's the first element
			new List<ArbitraryClass> { new ArbitraryClass(), new ArbitraryClass() }.MaxBy(i => i.ID, comparer.Object);

			comparer.Verify(c => c.Compare(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
		}

		[Fact]
		public static void MinBy_Class_ReturnsNullOnEmptyList() {
			var actual = new List<ArbitraryClass>().MinBy(i => i.ID);

			Assert.Null(actual);
		}

		[Fact]
		public static void MinBy_Struct_ReturnsDefaultOnEmptyList() {
			var actual = new List<ArbitraryStruct>().MinBy(i => i.ID);

			Assert.Equal(default(ArbitraryStruct), actual);
		}

		[Fact]
		public static void MinBy_Class_SelectsMaximumBySelector() {
			var list = new List<ArbitraryClass> {
				new ArbitraryClass {
					Name = "b",
					ID = 1
				},
				new ArbitraryClass {
					Name = "a",
					ID = 2
				}
			};
			var expectedName = "b";

			var actualName = list.MinBy(i => i.ID).Name;

			Assert.Equal(expectedName, actualName);
		}

		[Fact]
		public static void MinBy_Struct_SelectsMaximumBySelector() {
			var list = new List<ArbitraryStruct> {
				new ArbitraryStruct {
					Name = "b",
					ID = 1
				},
				new ArbitraryStruct {
					Name = "a",
					ID = 2
				}
			};
			var expectedName = "b";

			var actualName = list.MinBy(i => i.ID).Name;

			Assert.Equal(expectedName, actualName);
		}

		[Fact]
		public static void MinBy_DuplicateEntries_ReturnsFirst() {
			var list = new List<ArbitraryClass> {
				new ArbitraryClass {
					Name = "a",
					ID = 1
				},
				new ArbitraryClass{
					Name = "b",
					ID = 1
				},
				new ArbitraryClass {
					Name = "c",
					ID = 2
				}
			};
			var expectedName = "a";

			var actualName = list.MinBy(i => i.ID).Name;

			Assert.Equal(expectedName, actualName);
		}

		[Fact]
		public static void MinBy_CustomComparerUsed() {
			var comparer = new Mock<IComparer<int>>();
			// add two objects, since a one-element list short-circuts the comparison because it's the first element
			new List<ArbitraryClass> { new ArbitraryClass(), new ArbitraryClass() }.MinBy(i => i.ID, comparer.Object);

			comparer.Verify(c => c.Compare(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
		}

		[Fact]
		public static void NullSafeSequenceEqual_SameLists_ReturnTrue() {
			var list = new List<int>();

			Assert.True(list.NullSafeSequenceEqual(list));
		}

		[Fact]
		public static void NullSafeSequenceEqual_NullLists_ReturnsTrue() {
			List<int> list = null;

			Assert.True(list.NullSafeSequenceEqual(null));
		}

		[Fact]
		public static void NullSafeSequenceEqual_JustListANull_ReturnsFalse() {
			List<int> a = null;
			List<int> b = new List<int>();

			Assert.False(a.NullSafeSequenceEqual(b));
		}

		[Fact]
		public static void NullSafeSequenceEqual_JustListBNull_ReturnsFalse() {
			List<int> a = new List<int>();
			List<int> b = null;

			Assert.False(a.NullSafeSequenceEqual(b));
		}

		[Fact]
		public static void NullSafeSequenceEqual_SameLists_ReturnsTrue() {
			var list1 = new[] { 1, 2, 3 };
			var list2 = new[] { 1, 2, 3 };

			Assert.True(list1.NullSafeSequenceEqual(list2));
		}

		[Fact]
		public static void NullSafeSequenceEqual_DifferentLists_ReturnsFalse() {
			var list1 = new[] { 1, 2, 3 };
			var list2 = new[] { 4, 5, 6 };

			Assert.False(list1.NullSafeSequenceEqual(list2));
		}

		[Fact]
		public static void NullSafeSequenceEqual_CustomComparer_ComparerUsed() {
			var comparer = new Mock<IEqualityComparer<int>>();
			var list1 = new[] { 1, 2, 3 };
			var list2 = new[] { 1, 2, 3 };
			list1.NullSafeSequenceEqual(list2, comparer.Object);

			comparer.Verify(c => c.Equals(It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce());
		}

		[Fact]
		public static void ChunkUp_CreatesChunkSizedLists() {
			var source = Enumerable.Range(0, 50);

			var actual = source.ChunkUp(5).ToList();

			Assert.Equal(10, actual.Count);
			foreach (var chunk in actual) {
				Assert.Equal(5, chunk.Count());
			}
		}

		[Fact]
		public static void ChunkUp_IncludesExtraItems() {
			var source = Enumerable.Range(0, 51);

			var actual = source.ChunkUp(5).Last();

			Assert.Equal(1, actual.Count());
		}

		[Fact]
		public static void ChunkUp_IncludesAllItems_InSameOrder() {
			var original = Enumerable.Range(0, 51).ToList();

			var actual = original.ChunkUp(5).SelectMany(l => l).ToList();

			Assert.True(original.SequenceEqual(actual));
		}

		[Theory]
		[InlineData(0)]
		[InlineData(-1)]
		[InlineData(Int32.MinValue)]
		public static void ChunkUp_CountZero_ThrowsArgumentException_WithBadArgument(int size) {
			var exception = Assert.Throws<ArgumentException>(() => Enumerable.Range(1, 100).ChunkUp(size).ToList());
			Assert.Contains(size.ToString(), exception.Message);
		}

		[Fact]
		public static void DistinctBy_GetsFirstDistinctElements() {
			var list = new List<ArbitraryClass> {
				new ArbitraryClass {
					Name = "a",
					ID = 1
				},
				new ArbitraryClass {
					Name = "a",
					ID = 2
				},
				new ArbitraryClass {
					Name = "b",
					ID = 3
				},
				new ArbitraryClass {
					Name = "b",
					ID = 4
				}
			};
			var expected = list.Where(i => i.ID == 1 || i.ID == 3);

			var actual = list.DistinctBy(i => i.Name);

			Assert.True(expected.SequenceEqual(actual));
		}

		[Fact]
		public static void Permutations_List_ListsAllPermutations() {
			var list = new[] { 1, 2, 3 };
			var expected = new[]{
				new [] { 1, 2, 3 },
				new [] { 1, 3, 2 },
				new [] { 2, 1, 3 },
				new [] { 2, 3, 1 },
				new [] { 3, 1, 2 },
				new [] { 3, 2, 1 }
			};

			var actual = list.Permutations().ToList();

			Assert.True(actual.All(permutation => expected.Count(expectation => expectation.SequenceEqual(permutation)) == 1));
			Assert.Equal(expected.Length, actual.Count);
		}

		[Fact]
		public static void Permutations_EmptyList_ReturnsEmptyList() {
			var actual = new int[0].Permutations().ToList();

			Assert.Empty(actual);
		}

		[Fact]
		public static void Permutations_DuplicatedItemInList_ReturnsPermutations() {
			var list = new[] { 1, 1 };
			var expected = new[] {
				new[] { 1, 1 },
				new[] { 1, 1 }
			};

			var actual = list.Permutations().ToList();

			Assert.Equal(expected.Length, actual.Count);
			for (int i = 0; i < expected.Length; ++i) {
				Assert.Equal(expected[i], actual[i]);
			}
		}

		[Fact]
		public static void UInt64Count_ReturnsCount() {
			var list = new[] { 1, 2, 3 };
			var expected = (UInt64)list.Length;

			var actual = list.UInt64Count();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public static void UInt64Count_ObeysSelector() {
			var list = new[] { 1, 2, 3, 4, 5 };
			var selector = new Func<int, bool>(i => i % 2 == 0);
			var expected = (UInt64)list.Count(selector);

			var actual = list.UInt64Count(selector);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public static void Flatten_SingleElement_ReturnsElement() {
			var expected = ArbitraryClass.GenerateRandomInstance();

			var actual = new[] { expected }.Flatten(i => i.Children).ToList();

			Assert.Equal(1, actual.Count);
			Assert.Same(expected, actual[0]);
		}

		[Fact]
		public static void Flatten_OneLevelDeep_ReturnsAllElements() {
			var children = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var parent = ArbitraryClass.GenerateRandomInstance();
			parent.Children = children;

			var actual = new[] { parent }.Flatten(i => i.Children).ToList();

			Assert.Equal(11, actual.Count);
			Assert.True(actual.Any(i => ReferenceEquals(parent, i)));
			Assert.All(children, c => actual.Any(i => ReferenceEquals(i, c)));
		}

		[Fact]
		public static void Flatten_TwoLevelsWithSomeNulls_ReturnsAllElements() {
			var grandChildren1 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var grandChildren2 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var grandChildren3 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var grandChildren4 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var grandChildren5 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var grandChildren6 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var children1 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var children2 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var children3 = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			var parents = Enumerable.Range(1, 10).Select(i => ArbitraryClass.GenerateRandomInstance()).ToList();
			parents[0].Children = children1;
			parents[0].Children[1].Children = grandChildren1;
			parents[0].Children[2].Children = grandChildren2;
			parents[2].Children = children2;
			parents[2].Children[1].Children = grandChildren3;
			parents[2].Children[2].Children = grandChildren4;
			parents[6].Children = children3;
			parents[6].Children[1].Children = grandChildren5;
			parents[6].Children[2].Children = grandChildren6;
			var expectedish = grandChildren1.Concat(grandChildren2).Concat(grandChildren3).Concat(grandChildren4).Concat(grandChildren5).Concat(grandChildren6)
				.Concat(children1).Concat(children2).Concat(children3)
				.Concat(parents)
				.ToList();

			var actual = parents.Flatten(i => i.Children).ToList();

			Assert.Equal(expectedish.Count, actual.Count);
			Assert.All(expectedish, e => actual.Any(a => ReferenceEquals(e, a)));
		}
	}
}