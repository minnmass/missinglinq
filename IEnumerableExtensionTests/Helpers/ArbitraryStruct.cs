﻿namespace MissingLinqTests.Helpers {
	public class ArbitraryStruct {
		public string Name { get; set; }
		public int ID { get; set; }
	}
}
