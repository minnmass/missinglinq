﻿using System;
using System.Collections.Generic;

namespace MissingLinqTests.Helpers {
	public class ArbitraryClass {
		private static Random Random = new Random();

		public string Name { get; set; }
		public int ID { get; set; }
		public List<ArbitraryClass> Children { get; set; }

		public static ArbitraryClass GenerateRandomInstance() {
			return new ArbitraryClass {
				Name = Guid.NewGuid().ToString(),
				ID = Random.Next()
			};
		}
	}
}
