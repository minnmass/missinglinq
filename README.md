# README #

This is a library of Linq-type helpers that I've had to reimplement multiple times.

Pull requests are welcome. Available on [Nuget](https://www.nuget.org/packages/MissingLinq.Robert_Fensterman/).

# Included Methods #

## MaxBy ##

Gets the element with the largest value for the selection (the first one it comes across, if there are ties).

Uses the default comparer if none is passed in.

## MinBy ##

Same as MaxBy, but gets the minimum value.

## NullSafeSequenceEqual ##

Returns true if both lists are null or if they're both non-null and are SequenceEqual. Critically, does _not_ throw a NullReferenceException if either list is null.

## ChunkUp ##

Chunks the list up into bite-sized pieces. Useful if an API can handle multiple elements per request, but only a few at a time.

## DistinctBy ##

Gets a subset of the list for which the selection is distinct for all items (gets the first such item, if there are multiples).

Logically simlar to ```source.GroupBy(i => selector(i)).Select(g => g.First());```, but it doesn't actually create groups.

## Permutations ##

Generates a list containing all permutations of the given list.

## UInt64Count ##

Gets the count of items in the list, even if the list is exceedingly long. Increments the counter in a [checked](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/checked) context so overflows throw an exception.
