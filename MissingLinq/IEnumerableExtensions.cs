﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MissingLinq {
	public static class IEnumerableExtensions {
		public static TSource MaxBy<TSource, TVal>(this IEnumerable<TSource> source, Func<TSource, TVal> selector, IComparer<TVal> comparer = null) {
			return MinMaxByBody(source, selector, GreaterThan, comparer);
		}

		public static TSource MinBy<TSource, TVal>(this IEnumerable<TSource> source, Func<TSource, TVal> selector, IComparer<TVal> comparer = null) {
			return MinMaxByBody(source, selector, LessThan, comparer);
		}

		private static TSource MinMaxByBody<TSource, TVal>(IEnumerable<TSource> source, Func<TSource, TVal> selector, Func<TVal, TVal, IComparer<TVal>, bool> comparisonType, IComparer<TVal> comparer = null) {
			TVal keySelector = default(TVal);
			TSource keyValue = default(TSource);

			var enumerator = source.GetEnumerator();
			do {
				keyValue = enumerator.Current;
			} while (keyValue == null && enumerator.MoveNext());

			if (keyValue != null) {
				keySelector = selector(keyValue);
			} else {
				return keyValue; // functionally returns null, since we clearly broke out of the loop via enumerator.MoveNext() running out of items
			}

			comparer = comparer ?? Comparer<TVal>.Default;

			while (enumerator.MoveNext()) {
				var item = enumerator.Current;
				if (item != null) {
					var selection = selector(item);
					if (comparisonType(selection, keySelector, comparer)) {
						keyValue = item;
						keySelector = selection;
					}
				}
			}

			return keyValue;
		}

		private static bool LessThan<T>(T a, T b, IComparer<T> comparer) {
			return comparer.Compare(a, b) < 0;
		}

		private static bool GreaterThan<T>(T a, T b, IComparer<T> comparer) {
			return comparer.Compare(a, b) > 0;
		}

		/// <summary>
		/// Determine if lists are sequence-equal; if both are null, return true; if exactly one is null, return false
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="comparer"></param>
		/// <returns></returns>
		public static bool NullSafeSequenceEqual<T>(this IEnumerable<T> a, IEnumerable<T> b, IEqualityComparer<T> comparer = null) {
			if (ReferenceEquals(a, b)) { return true; }
			if (a == null || b == null) { return false; }
			if (comparer == null) {
				return a.SequenceEqual(b);
			}
			return a.SequenceEqual(b, comparer);
		}

		/// <summary>
		/// Break up a list into chunks of at most "count" items.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="count"></param>
		/// <exception cref="ArgumentException">Throws when count is less than 1.</exception>
		/// <returns></returns>
		public static IEnumerable<IEnumerable<T>> ChunkUp<T>(this IEnumerable<T> source, int count) {
			if (count < 1) {
				throw new ArgumentException($"Error: cannot break enumerable into chunks of size {count}; must be 1 or greater.");
			}
			var list = new List<T>(count);

			foreach (var item in source) {
				list.Add(item);
				if (list.Count == count) {
					yield return list;
					list = new List<T>(count);
				}
			}

			if (list.Any()) {
				yield return list;
			}
		}

		/// <summary>
		/// Yield the first item in source for each distinct value returned by the selector. Uses HashSet.Add() to determine uniqueness.
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TVal"></typeparam>
		/// <param name="source"></param>
		/// <param name="selector"></param>
		/// <returns></returns>
		public static IEnumerable<TSource> DistinctBy<TSource, TVal>(this IEnumerable<TSource> source, Func<TSource, TVal> selector) {
			var seen = new HashSet<TVal>();

			foreach (var item in source) {
				if (seen.Add(selector(item))) {
					yield return item;
				}
			}
		}

		/// <summary>
		/// Generate all permutations of the given IEnumerable.
		/// Note: duplicate items in the input will be treated as separate instances; Distinct()-ify first if need be
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <returns></returns>
		public static IEnumerable<IEnumerable<T>> Permutations<T>(this IEnumerable<T> list) {
			var l = list.ToList();

			if (l.Count == 0) {
				return Enumerable.Empty<IEnumerable<T>>();
			}

			return Permutations(Enumerable.Range(0, l.Count).ToList(), l.Count)
				.Select(il => il.Select(idx => l[idx]));

			IEnumerable<IEnumerable<TInner>> Permutations<TInner>(IEnumerable<TInner> innerList, int length) {
				// http://stackoverflow.com/a/10630026
				if (length == 1) {
					return innerList.Select(t => new TInner[] { t });
				}

				return Permutations(innerList, length - 1)
					.SelectMany(
						t => innerList.Where(e => !t.Contains(e)),
						(t1, t2) => t1.Concat(new TInner[] { t2 })
					);
			}
		}

		/// <summary>
		/// Get the count of elements in the list that match the selector. Throws an exception if there are too many in the list.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <param name="selector"></param>
		/// <returns></returns>
		public static UInt64 UInt64Count<T>(this IEnumerable<T> list, Func<T, bool> selector) {
			return list.Where(selector).UInt64Count();
		}

		/// <summary>
		/// Get the count of elements in the (extremly long) list that match the selector.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <param name="selector"></param>
		/// <exception cref="OverflowException">If there are more than UInt64.MaxValue items</exception>
		/// <returns></returns>
		public static UInt64 UInt64Count<T>(this IEnumerable<T> list) {
			UInt64 count = 0;

			foreach (var item in list) {
				count = checked(count + 1);
			}

			return count;
		}

		/// <summary>
		/// Get all descendents of an object which contains a list-type container of itself (basically, a recursive SelectMany)
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name=""></param>
		/// <param name="selector"></param>
		/// <returns></returns>
		public static IEnumerable<T> Flatten<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> selector) {
			if (source != null) {
				foreach (var element in source) {
					yield return element;
					if (element != null) {
						foreach (var result in selector(element).Flatten(selector)) {
							yield return result;
						}
					}
				}
			}
		}
	}
}